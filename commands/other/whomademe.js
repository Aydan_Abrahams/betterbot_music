const { MessageEmbed } = require('discord.js');
const { Command } = require('discord.js-commando');

module.exports = class WhoMadeMeCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'whomademe',
      aliases: ['bot-maker', 'bot-creator'],
      memberName: 'whomademe',
      group: 'other',
      description: "Replies with the bot creator's name"
    });
  }

  run(message) {
    const WhoMadeMeEmbed = new MessageEmbed()
      .setColor('#ff0000')
      .setTitle('Made with :heart: by `@NEOHAMS#0570`')
      message.channel.send(WhoMadeMeEmbed);
    }
};
