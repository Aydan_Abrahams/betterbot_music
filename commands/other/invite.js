const { MessageEmbed } = require('discord.js');
const { Command } = require('discord.js-commando');

module.exports = class InviteCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'invite',
      aliases: ['inv'],
      memberName: 'invite',
      group: 'other',
      description: "Invite me to your server"
    });
  }

  run(message) {
  const InviteEmbed = new MessageEmbed()
	  .setColor('#ff0000')
    .setTitle('Invite Me To Your Discord :)')
    .setDescription('https://discord.com/oauth2/authorize?client_id=719137762384674967&scope=bot&permissions=36949248');
    message.channel.send(InviteEmbed);
  }
};
